-- Monk's first weapon on quest 30027

INSERT INTO gameobject_loot_template (Entry, Item, Reference, Chance, QuestRequired, LootMode, GroupId, MinCount, MaxCount, Comment) VALUES
(40856, 77278, 0, 100, 1, 1, 0, 1, 1, 'Trainee\'s Handwrap'),
(40856, 77279, 0, 100, 1, 1, 0, 1, 1, 'Trainee\'s Handwrap (Left Hand)');